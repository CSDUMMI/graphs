Usage: navigator [options] [filename] [start] [end]

options:
--draw -d           Draw the graph with the paths found
--algorithms -a     Specify the path finding algorithms to use [default: quick, short, quick-vert]

arguments:
filename            JSON File containing a description of the Graph
start               Label of the starting node
end                 Label of the destination node

# Drawings
The drawings show the edge weights in between the vertices.

The label of a vertex (a unique number) is shown below the weight
of that vertex.

# Path finding modes
There are five modes for calculating the paths between two points:
1. `quick`  The fastest route according to edge weights only. (darkgreen)
2. `short`  The route with the fewest vertices. [magenta)
3. `quick-vert` The route with the lowest vertex weights [blue]
4. `combined-quick-short`   Combination of the fastest route according to the edge weights and the shortest route. [yellow]
5. `combined-quick-quick-vert`  The fastest route according to edge weights and vertex weights [red]

# Write Jason
Example FILE:
{
  "edges" : [[1, 2, 3], [2, 3, 4]],
  "vertex-weights" : [[1, 4], [2, 24]]
}

edges: List of a List of edges; an edge is a list of the form (from to weight)
vertex-weights: List of a list of the form (label weight) of a vertex
