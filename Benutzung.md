# Benutzung
Unser Navigator kann sowohl über die Kommandozeile (durch die Datei Graph/Cmd.rkt) oder
in Racket angesteurt, indem `run` in der Datei `Main.rkt` mit den gleichen Optionen ausgeführt
wird, die auch durch die Kommandozeile übergeben werden.

### Usage
```
$ racket Cmd.rkt --help
usage: navigator [ <option> ... ] <filename> <start> <end>

<option> is one of

  -a <algs>, --algorithms <algs>
     Select algorithms to calculate paths with [default: quick,short,quick-vert,quick<>short,quick<>quick-vert]
  -d, --draw
     Draw the graph with the calculated paths
  --help, -h
     Show this help
  --
     Do not treat any remaining argument as a switch (at this level)

 Multiple single-letter switches can be combined after
 one `-`. For example, `-h-` is the same as `-h --`.
```
# Bildliche Darstellung
Die Bildliche Darstellung zeigt die Knoten als Kreise und die Kanten als ihre Verbindungslinien. Der Startknoten wird grün und der Zielknoten rot angezeigt. Das Kantengewicht ist in der Mitte der Kante angegeben. Das Gewicht des Knoten ist oben im Kreis und sein Label unten angezeigt. Der Pfad wird je nach ausgewählten Modi in der Bestimmten Farbe angegeben(siehe Modus des Suchverfahrens)


# Modus des Suchverfahrens
Dies sind fünf Modi, um den Pfad zwischen zwei Knoten zu berechnen
1. `quick`  Der schnellste Weg nach den Kantengewichten (darkgreen)
2. `short`  Der Weg mit den geringsten Knoten [magenta)
3. `quick-vert` Der schnellste Weg nach den Knotengewichten [blue]
4. `quick<>short`   Kombination aus `quick` und `short`: kürzester Weg, wenn die Anzahl an Knoten zu der Länge des Weges nach den Kantengewichten addiert wird [yellow]
5. `quick<>quick-vert` Kombination aus `quick` und `quick-vert`: kürzester Weg, wenn die Länge des Weges nach den Kantengewichten mit der Länge des Weges nach den Knotengewichten addiert wird [red]

# Jason Datei
Beispiel:
```json
{
  "edges" : [[1, 2, 3], [2, 3, 4]],
  "vertex-weights" : [[1, 4], [2, 24]]
}
```

edges: Liste von Kanten mit den Informationen (from to weight)
vertex-weights: Liste von Knoten (label weight) 
